﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdaptiveAudio : MonoBehaviour
{
    public enum MusicStates { Menu, StandartGameplay, HurryUp, EndGame };
    private MusicStates musicState;

    public AudioSource BaseMusic;
    public AudioSource FastRythms;

    void Start()
    {
        
        musicState = MusicStates.StandartGameplay;
        BaseMusic.Play();
    }

    
    void Update()
    {
        if(musicState == MusicStates.StandartGameplay)
        {
            FastRythms.Stop();
        }
    }

    public void MenuMusic()
    {

    }

    public void StandartMusic()
    {
        musicState = MusicStates.StandartGameplay;
        
    }

    public void FasterMusic()
    {
        musicState = MusicStates.HurryUp;
        FastRythms.Play();
    }

    public void EndGameMusic()
    {

    }
}
